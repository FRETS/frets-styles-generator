# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/FRETS/frets/compare/v0.1.9...v0.2.0) (2019-01-23)


### Features

* custom configs and default template ([5ef0769](https://gitlab.com/FRETS/frets/commit/5ef0769))



<a name="0.1.9"></a>
## [0.1.9](https://gitlab.com/FRETS/frets/compare/v0.1.8...v0.1.9) (2018-11-14)



<a name="0.1.8"></a>
## [0.1.8](https://gitlab.com/FRETS/frets/compare/v0.1.7...v0.1.8) (2018-11-13)



<a name="0.1.7"></a>
## [0.1.7](https://gitlab.com/FRETS/frets/compare/v0.1.6...v0.1.7) (2018-11-13)



<a name="0.1.6"></a>
## [0.1.6](https://gitlab.com/FRETS/frets/compare/v0.1.5...v0.1.6) (2018-07-31)



<a name="0.1.5"></a>
## [0.1.5](https://gitlab.com/FRETS/frets/compare/v0.1.4...v0.1.5) (2018-07-31)



<a name="0.1.4"></a>
## [0.1.4](https://gitlab.com/FRETS/frets/compare/v0.1.2...v0.1.4) (2018-04-28)



<a name="0.1.3"></a>
## [0.1.3](https://gitlab.com/FRETS/frets/compare/v0.1.2...v0.1.3) (2017-11-29)

updated h method to support maquette 3 method signature


<a name="0.1.2"></a>
## 0.1.2 (2017-11-29)
